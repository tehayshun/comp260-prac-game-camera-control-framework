﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMove : MonoBehaviour {

    public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 10.0f; // in metres/second/second
	public float brake = 5.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second

	private Rigidbody2D rigidbody;

	void Start() {
		rigidbody = GetComponent<Rigidbody2D>();
	}

    void FixedUpdate() {

		Vector2 heading = transform.up;
		float speed = Vector2.Dot(rigidbody.velocity, heading);

		// the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");

		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} 
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} 
		else {
			// braking
			if (speed > 0) {
				speed = Mathf.Max(0, speed - brake * Time.deltaTime);
			} else {
				speed = Mathf.Min(0, speed + brake * Time.deltaTime);
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal");

		// turn the car
		rigidbody.angularVelocity = -turn * turnSpeed * speed / maxSpeed;

        // compute a vector in the up direction of length speed
		rigidbody.velocity = heading * speed;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // get the direction in which to shake
        Vector3 normal = collision.contacts[0].normal;
        CameraShake cameraShake =
        Camera.main.GetComponent<CameraShake>();
        cameraShake.Shake(normal);
    }

}
